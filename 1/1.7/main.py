import warmup

def main():
    print("Sum numbers:")
    print(warmup.sum_numbers([1, 2, 3, 4, 5]))

    
    print("Replace char:")
    print(warmup.replace_char("abc", "a", "b"))

    print("Sum from file:")
    print(warmup.sum_from_file('numbers_1.txt'))
    
    print("Sum of most frequent:")
    print(warmup.sum_of_most_frequent([2, 2, 3, 4, 5]))
    
    print("Matrix replace:")
    print(warmup.matrix_replace([[2, 2, 3, 4, 5],[2, 2, 3, 4, 5],[2, 2, 3, 4, 5],[2, 2, 3, 4, 5],[2, 2, 3, 4, 5]]))

if __name__ == '__main__':
    main()
