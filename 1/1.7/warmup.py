def sum_numbers(numbers):
    '''
    Return the sum of the integers in the given list.

    Parameters
    ----------
    numbers (list of int) : list of integers to be summed

    Returns
    -------
    the sum (int)

    for example the function call sum_numbers([1,2,3,4,5])
    should return 15
    '''
    
    sum = 0
    for i in numbers:
        sum +=i
    
    return sum


def replace_char(string, old, new):
    '''
    Return a new string in which the given __string__s __old__ character has been replaced by the __new__ character.
    You can assume that the length of the __old__ and __new__ parameters is one character.

    Parameters
    ----------
    string (string) : a string of characters
    old (string) : a character that will be replaced by new character
    new (string) : a character that replaces the old character

    Returns
    -------
    the new string

    for example the function call replace_char("aabbaac", "a", "e")
    should return "eebbeec".
    '''
    
    ret = string.replace(old,new)
    return ret        


def sum_from_file(filename):
    '''
    Return the sum of the integers in the given file.
    You can assume that the numbers in the given file are on separate lines (see numbers_1.txt).
    The function should return 0 if the file is empty.

    The file 'numbers_1.txt' is included in the zip for testing purposes,
    you can also make your own test files.

    Parameters
    ----------
    filename (string) : name of the file

    Returns
    -------
    the new string

    for example the function call sum_from_file("numbers_1.txt")
    should return 17
    '''

    f = open(filename, "r")
    
    sum = 0
    for i in f:
        sum += int(i)
    
    return sum

        
def sum_of_most_frequent(numbers):
    '''
    Return the most frequent number in given list multiplied by its frequency.

    Parameters
    ----------
    numbers (list of int) : a list of integers

    Returns
    -------
    int

    For example the function call sum_of_most_frequent([1,5,2,7,1,2,2,3,2,3,2,2,2])
    should return 14
    because 2 is the most frequent number and it occurs 7 times in the given list.
    '''
    
    most_frequent_occur = 0
    for i in numbers:
        j = numbers.count(i)
        if j > most_frequent_occur:
            most_frequent = i
            most_frequent_occur = j
            
    ret = most_frequent * most_frequent_occur
    return ret    


def matrix_replace(matrix):
    '''
    Loop through the given two-dimensional list, __matrix__, and replace all the odd numbers with 0,
    while leaving even numbers as they were. Return the new two-dimensional list.

    Parameters
    ----------
    matrix (two-dimensional list of int) : a two-dimensional list of integers

    Returns
    -------
    two-dimensional list of int

    For example the function call matrix_replace([[1,2,3], [4,5,6], [7,8,9]])
    should return [[0, 2, 0], [4, 0, 6], [0, 8, 0]]
    '''

    for j in matrix:
        i = 0
        while i < len(j):
            if j[i] % 2 != 0:
                j[i] = 0
            i += 1    

    return matrix