import warmup
from warmup import students_example

def main():

    print("Students dictionary:")
    print(students_example)  # students_example is imported from warmup.py

    print("Highest homework score:")
    print(warmup.highest_homework_score(students_example))

    print("Students by points:")
    print(warmup.students_by_points(students_example, 9, 9))

    print("Students by grade:")
    print(warmup.students_by_grade(students_example))

if __name__ == '__main__':
    main()
