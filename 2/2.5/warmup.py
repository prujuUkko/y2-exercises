
# Example students dictionary

students_example = {
    1111: {
        'name': 'Tiina Teekkari',
        'homework': [6, 7, 9, 5],
        'exam' : 38,
        'grade' : 3
    },
    2222: {
        'name': 'Matti Meikalainen',
        'homework': [9, 10, 10, 10],
        'exam' : 45,
        'grade' : 5
    },
    3333: {
        'name': 'Kalle Kemisti',
        'homework': [4, 3, 5, 7],
        'exam' : 10,
        'grade' : 1
    },
    4444: {
        'name': 'Teemu Teekkari',
        'homework': [3, 1, 2, 1],
        'exam' : 8,
        'grade' : 0
    }
}


def highest_homework_score(students):
    '''
    Return the name of the student with the highest homework score (the sum of all homework points).
    If multiple students share the same highest score, you can return any of them.
    See students_example above for an example of the __students__ parameter.

    Parameters
    ----------
    students (dictionary) : students dictionary

    Returns
    -------
    the name (string)

    For example the function call highest_homework_score(students_example)
    should return 'Matti Meikalainen'
    '''
    largest = 0
    
    for i in students.items():
        sum = 0
       
        for j in i[1]['homework']:
            sum += j 
        if sum > largest:
            largest = sum 
            ret = i[1]['name']  

    return ret 

def students_by_points(students, exam_limit, homework_limit):
    '''
    Return a list of the student names who got __homework_limit__ or more points in all of the homework rounds
    and __exam_limit__ or more in the exam. See students_example above for an example of the __students__ parameter.

    Parameters
    ----------
    students (dictionary) : students dictionary
    exam_limit (int) : the limit that the student has to at least get from the exam
    homework_limit (int) : the limit that the student has to at least get from each homework round

    Returns
    -------
    a list of strings

    For example the function call students_by_points(students_example, 25, 3)
    should return ['Matti Meikalainen', 'Tiina Teekkari']
    '''
    ret = []
    homework_sum = 0 
    
    for i in students.items():
        homework_pass = True
        for j in i[1]['homework']:
           if j < homework_limit:
                homework_pass = False
                break
        
        if homework_pass == True and i[1]['exam'] >= exam_limit:
            ret.append(i[1]['name'])            
            
    
    return ret

def students_by_grade(students):
    '''
    Return a dictionary associating each grade {0, 1, 2, 3, 4, 5} to a set of students who obtained that grade.
    If no students obtained some grade then an empty set should be associated to it.
    See students_example above for an example of the __students__ parameter.

    Parameters
    ----------
    students (dictionary) : students dictionary

    Returns
    -------
    dictionary

    For example the function call students_by_grade(students_example)
    should return {0: {'Teemu Teekkari'}, 1: {'Kalle Kemisti'}, 2: set(),
                   3: {'Tiina Teekkari'}, 4: set(), 5: {'Matti Meikalainen'}}
    '''
    ret = {0: set(), 1: set(), 2: set(), 3: set(), 4: set(), 5: set()}
    
    for i in students.items():
        if i[1]['grade'] == 0:
            ret[0].add(i[1]['name'])
            
        if i[1]['grade'] == 1:
            ret[1].add(i[1]['name'])
        
        if i[1]['grade'] == 2:
            ret[2].add(i[1]['name'])
        
        if i[1]['grade'] == 3:
            ret[3].add(i[1]['name'])
        
        if i[1]['grade'] == 4:
            ret[4].add(i[1]['name'])
        
        if i[1]['grade'] == 5:
            ret[5].add(i[1]['name'])
            
    return ret 
