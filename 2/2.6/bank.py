from customer import Customer
from account import Account
from personal_account import PersonalAccount
from savings_account import SavingsAccount

class Bank:
    def __init__(self, name):
        self.name = name
        self.customers = []
        
    def get_name(self):
        return self.name 
    
    def get_customers(self):
        return self.customers
    
    def get_customers_by_name(self, name):
        ret = []
        for i in self.customers:
            if i.name == name:
                ret.append(i)
            
        return ret 
    
    def get_customer_by_id(self, id):
        for i in self.customers:
            if i.id == id:
                return i 
    
    def add_customer(self, customer):
        if customer not in self.customers:
            self.customers.append(customer)
    
    def remove_customer(self, customer):
        if customer in self.customers:
            self.customers.remove(customer)            
        
    def add_savings_account(self, customer):
        if customer not in self.customers:
            return None
        
        else:
            i = 0
            while i < len(self.customers):
                if self.customers[i] == customer:
                    break
                i += 1
        new_account = SavingsAccount(customer)
        self.customers[i].savings_accounts.append(new_account)
        
        return self.customers[i].savings_accounts[len(self.customers[i].savings_accounts) - 1] 
    
    def add_personal_account(self, customer):
        if customer not in self.customers:
            return None
        
        else:
            i = 0
            while i < len(self.customers):
                if self.customers[i] == customer:
                    break
                i += 1
        new_account = PersonalAccount(customer)    
        self.customers[i].personal_accounts.append(new_account)
        
        return self.customers[i].personal_accounts[len(self.customers[i].personal_accounts)-1]
    
    def  get_accounts(self, customer):
        ret = customer.personal_accounts + customer.savings_accounts
        return ret 
        