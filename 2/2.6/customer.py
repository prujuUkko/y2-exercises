class Customer:
    id_number = 1
    
    def __init__(self, name):
        self.name = name
        self.id = Customer.id_number
        Customer.id_number += 1
        self.personal_accounts = []
        self.savings_accounts = []
    
    def get_name(self):
        return self.name 
    
    def get_id(self):
        return self.id
    
    
    
