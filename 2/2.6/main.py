from bank import Bank
from customer import Customer

def main():
    bank = Bank("The Best Bank")
    c1 = Customer("Happy Customer")
    c2 = Customer("Annoying Customer")
    bank.add_customer(c1)
    bank.add_customer(c2)
    account_1 = bank.add_personal_account(c1)
    account_2 = bank.add_personal_account(c2)
    account_1.deposit(100)

    print_balance(account_1)
    print_balance(account_2)
    print("")

    sum_to_transfer = 5
    
    if account_1.transfer_to(account_2, sum_to_transfer):
        print("Transfer of {} complete!")
    else:
        print("Transfer of {} failed!")

    print("")
    print_balance(account_1)
    print_balance(account_2)
    
    print(bank.get_customer_by_id(1).name)

def print_balance(account):
    print("{} has a balance of {}".format(account.get_customer().get_name(),
                                          account.get_balance()))

if __name__ == '__main__':
    main()
