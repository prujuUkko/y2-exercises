from account import Account
class PersonalAccount(Account):
    def __init__(self, customer):
        Account.__init__(self, customer)
    
    def transfer_to(self, account, sum):
        if (self.balance - sum) >= 0:
            account.balance += sum
            self.balance -= sum
            return True
        else:
            return False
        