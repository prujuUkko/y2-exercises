import unittest
from datetime import date
from weekdays import weekdays


class TestWeekdays(unittest.TestCase):
    """Implement tests for weekdays here."""   
    def test_1(self):
        start = date(2018,1,1)
        end = date(2018,1,2)
        self.assertEqual(weekdays(start, end), [date(2018,1,1),date(2018,1,2)])
        

    def test_2(self):
        start = date(2018,1,5)
        end = date(2018,1,7)
        self.assertEqual(weekdays(start, end), [date(2018,1,5)])
         


if __name__ == '__main__':
    unittest.main()
