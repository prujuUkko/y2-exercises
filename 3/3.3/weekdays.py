from datetime import timedelta


def weekdays(start, end):
    '''
    Parameters start and end are date objects.
    Returns a sequence of all dates between
    start and end inclusively that are
    weekdays (Mon, Tue, Wed, Thu, Fri).
    '''
    result = []
    if(start > end):
        return result
    current = start
    oneday = timedelta(days=1)
    while True:
        if current.weekday() <= 4:  #Should be <= 4, 5 == sat
            result.append(current)
        current = current + oneday
        if current > end:
            return result


