import unittest


from linked_list import LinkedListException, LinkedList, Empty, Cell


class TestIsEmptyAndLength(unittest.TestCase):
    '''Täydennä tähän testit is_empty ja length -metodeille'''
    def test1(self):
        self.assertEqual(Empty.is_empty(Empty), True)
    
    def test2(self):
        self.assertEqual(Cell.is_empty(Cell), False)

    def test3(self):
        self.assertEqual(Empty.length(Empty), 0)
        
    def test4(self):
        empty = Empty()
        a = Cell("a", empty)
        b = Cell("b", a)
        self.assertEqual(Cell.length(b), 2)

if __name__ == '__main__':
    unittest.main(verbosity=2)
