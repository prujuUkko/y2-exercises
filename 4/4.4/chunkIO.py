from game import Game
from corrupted_chess_file_error import *
from player import Player
from piece import Piece
from board import Board

class ChunkIO(object):

    def load_game(self, input):

        '''
        @note:This is the game object this method will fill with data. The object
               is returned when the END chunk is reached.
        '''

        self.game = Game()

        try:

            # Read the file header and the save date
            
            header = self.read_fully(8, input)
            date = self.read_fully(8, input)

            # Process the data we just read.
            # NOTE: To test the line below you must test the class once with a broken header
            
            header = ''.join(header)
            date = ''.join(date)
            if not str(header).startswith("SHAKKI"):
                raise CorruptedChessFileError("Unknown file type")

            # The version information and the date are not used in this
            # exercise

            # *************************************************************
            #
            # EXERCISE
            #
            # ADD CODE HERE FOR READING THE
            # DATA FOLLOWING THE MAIN HEADERS
            #
            #
            # *************************************************************
           
            board = Board() #create board
            self.game.set_board(board)
            
            while True:
                
                type_and_len = self.read_fully(5, input)
                
                type = self.extract_chunk_name(type_and_len)
                len = self.extract_chunk_size(type_and_len) 
                
                if type == "CMT": 
                    self.CMT_reader(len, input)
                    continue  
                
                elif type == "PLR":
                    self.PLR_reader(len, input)    
                    continue
                
                elif type == "END":
                    if self.game._black and self.game._white:
                        break
                    else:
                        raise CorruptedChessFileError("Corrupted file")
                        
                else: #unknown chunk
                    self.unknown_chunk_reader(len, input)
                    continue
                    
            # If we reach this point the Game-object should now have the proper players and
            # a fully set up chess board. Therefore we might as well return it.
            
            return self.game

        except OSError:

            # To test this part the stream would have to cause an
            # OSException. That's a bit complicated to test. Therefore we have
            # given you a "secret tool", class BrokenReader, which will throw
            # an OSException at a requested position in the stream.
            # Throw the exception inside any chunk, but not in the chunk header.            
            raise CorruptedChessFileError("Reading the chess data failed 1.")




    # HELPER METHODS -------------------------------------------------------



    def extract_chunk_size(self, chunk_header):
        '''
        Given a chunk header (an array of 5 chars) will return the size of this
        chunks data.
        
        @param chunk_header:
                   a chunk header to process (str)
        @return: the size (int) of this chunks data
        '''

        
        # subtracting the ascii value of the character 0 from
        # a character containing a number will return the
        # number itself

        return int( ''.join( chunk_header[3:5] ) )


        '''
        Given a chunk header (an array of 5 chars) will return the name of this
        chunk as a 3-letter String.
        
        @param chunk_header:
                   a chunk header to process
        @return: the name of this chunk
        '''
    def extract_chunk_name(self, chunk_header):
        return ''.join( chunk_header[0:3] )
    
    def PLR_reader(self, lenght, input):
        color = self.read_fully(1,input) #read player color
        color = ''.join(color)
        
        name_len = self.read_fully(1, input) #read name length
        name_len = ''.join(name_len)
        
        name = self.read_fully(int(name_len), input) #read player name
        name =''.join(name)
        
        #create player object
        if color == "M":
            player = Player(name, Player.BLACK)
        if color == "V":
                player = Player(name, Player.WHITE)
                
        self.game.add_player(player) #add player to game
        
        pieces = self.read_fully(lenght-(2+int(name_len)), input) #read pieces, we've already read 7 + name_len
        pieces = ''.join(pieces)
         
        i = 0            
        while i < len(pieces): 
            piece_type = [0,0]
            piece_type[0] = pieces[i]
            piece_type[1] = pieces[i+1]
            i += 2
                        
            if piece_type[0] == "K" :
                piece_type = Piece.KING
                i -= 1
                        
            elif piece_type[0] == "D" :
                piece_type = Piece.QUEEN
                i -= 1 
                        
            elif piece_type[0] == "T" : 
                piece_type = Piece.ROOK
                i -= 1
                        
            elif piece_type[0] == "L" : 
                piece_type = Piece.BISHOP
                i -= 1
                        
            elif piece_type[0] == "R" :
                piece_type = Piece.KNIGHT
                i -= 1
                        
            elif not piece_type[0].isupper() and piece_type[1].isdigit: 
                piece_type = Piece.PAWN
                i -= 2  
                        
            else:
                raise CorruptedChessFileError("Corrupted file")        
            
            piece = Piece(player, piece_type) #create piece object
                    
            piece_coordinates = [0,0] #get piece coordinates
            piece_coordinates[0] = pieces[i]
            piece_coordinates[1] = pieces[i+1]
            i += 2
            
            column = Board.column_char_to_integer(piece_coordinates[0]) #convert column and row to index
            row = Board.row_char_to_integer(piece_coordinates[1])
            
            if (row > 7) or (column > 7): #check for corrupted column/row
                raise CorruptedChessFileError("Corrupted file")
            
            if self.game.board.get_piece(column, row) != None: #check if the place is already taken
                raise CorruptedChessFileError("Corrupted file")
            
            self.game._board.set_piece(piece, int(column), int(row))
    
    def CMT_reader(self, len, input):
        self.read_fully(int(len), input) #read CMT chunk
            
    def unknown_chunk_reader(self, len, input):
        self.read_fully(int(len), input) #
        
    def read_fully(self, count, input):
        '''
        The read-method of the Reader class will occasionally read only part of
        the characters that were requested. This method will repeatedly call read
        to completely fill the given buffer. The size of the buffer tells the
        algorithm how many bytes should be read.
        
        @param count:
                   How many characters are read
        @param input:
                   The character stream to read from
        @raises: OSError
        @raises: CorruptedChessFileError
        '''
        read_chars = input.read( count )
        
        # If the file end is reached before the buffer is filled
        # an exception is thrown.    
        if len(read_chars) != count:
                raise CorruptedChessFileError("Unexpected end of file.")

        return list(read_chars)
