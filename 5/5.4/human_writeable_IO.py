from game import Game
from corrupted_chess_file_error import *
from player import Player
from piece import Piece
from board import Board
from tkinter.constants import CURRENT

class HumanWriteableIO(object):

    def load_game(self, input):

        '''
        This is the game object this method will fill with data. The object
        is returned when the file ends and everything is ok.
        '''

        self.game = Game()
        board = Board()
        
        self.game.set_board(board)
        
        info_read = False
        white_read = False
        black_read = False

        
        #Use this variable for reading all the section headers.
        current_line = ''

        try:

            # Read the file header and the save date
            
            current_line = input.readline()
            header_parts = current_line.split(" ")
            
            # Process the data we just read.
            # NOTE: To test the line below you must test the class once with a
            # broken header

            
            if header_parts[0] != "SHAKKI":
                raise CorruptedChessFileError("Unknown file type")

            if header_parts[2].strip().lower() != 'tallennustiedosto':
                raise CorruptedChessFileError("Unknown file type")
            info_read = True

            # The version information and the date are not used in this
            # exercise
            
            # *************************************************************
            #
            # EXERCISE
            #
            # ADD CODE HERE FOR READING THE
            # DATA FOLLOWING THE MAIN HEADERS
            #
            #
            # *************************************************************

            self.piece_types = {'Kuningas':Piece.KING, 'Kuningatar':Piece.QUEEN, 'Torni':Piece.ROOK, 'Lahetti':Piece.BISHOP, 'Ratsu':Piece.KNIGHT, 'Sotilas':Piece.PAWN}
            
            black_pieces_bool = False
            white_pieces_bool = False
            
            for line in input:
                current_line = line
                current_line = current_line.strip() #strip whitespace
                
                if current_line.lower() == "#pelin tiedot" : #game info
                    self.read_game_info(input)
                    
                    if self.game._black == None or self.game._white == None:
                        raise CorruptedChessFileError("Corrupted chess file")
                
                elif current_line.lower() == "#musta": #black pieces
                    self.add_pieces(input, "b")
                    black_pieces_bool = True
                    
                elif current_line.lower() == "#valkoinen": #white pieces
                    self.add_pieces(input, "w")
                    white_pieces_bool = True
                    
                elif "#" in current_line: #other chunks
                    self.read_other_chunk(input)
            
            
            if black_pieces_bool == False or white_pieces_bool == False:
                raise CorruptedChessFileError("Corrupted chess file")
            
                
                
            # If we reach this point the Game-object should now have the proper
            # players and
            # a fully set up chess board. Therefore we might as well return it.
                
            
            
            return self.game
            
        except OSError:
            

            # To test this part the stream would have to cause an
            # OSError. That's a bit complicated to test. Therefore we have
            # given you a "secret tool", class BrokenReader, which will throw
            # an OSError at a requested position in the stream.
            # Throw the exception inside any chunk, but not in the chunk
            # header.

            raise CorruptedChessFileError("Reading the chess data failed.")

    def read_game_info(self, input):
        prev_pos = input.tell() #save previous pointer to go back one step when we hit the next chunk
        for line in input:
                prev_pos = input.tell() #save previous pointer to go back one step when we hit the next chunk
                current_line = line
                current_line = current_line.strip() #remove whitespace
                current_split = current_line.split(":") #split line
                current_split[0] = current_split[0].strip() #remove whitespace
                        
                if current_split[0].lower() == "tallennettu":
                    continue
                        
                elif current_split[0].lower() == "musta" :
                    current_split[1] = current_split[1].strip()
                    player = Player(current_split[1], Player.BLACK)
                    self.game.add_player(player)
                        
                elif current_split[0].lower() == "valkoinen" :
                    current_split[1] = current_split[1].strip()
                    player = Player(current_split[1], Player.WHITE)
                    self.game.add_player(player)
                        
                elif "#" in current_line: #we hit the next chunk
                    input.seek(prev_pos) #seek previous position
                    break
                
                prev_pos = input.tell() #save previous pointer to go back one step when we hit the next chunk
                
    def add_pieces(self, input, color):
        prev_pos = input.tell() #save previous pointer to go back one step when we hit the next chunk
        for line in input:
                current_line = line
                current_line = current_line.strip() #remove whitespace
                current_split = current_line.split(":") #split line
                current_split[0] = current_split[0].strip() #remove whitespace
                
                if current_line: #line not empty
                    
                    piece_type = None
                    
                    if current_split[0].lower() == "kuningas" :
                        piece_type = Piece.KING
                        
                    elif current_split[0].lower() == "kuningatar" :
                        piece_type = Piece.QUEEN
                        
                    elif current_split[0].lower() == "torni" :
                        piece_type = Piece.ROOK
                            
                    elif current_split[0].lower() == "lahetti" :
                        piece_type = Piece.BISHOP
                            
                    elif current_split[0].lower() == "ratsu" :
                        piece_type = Piece.KNIGHT
                        
                    elif current_split[0].lower() == "sotilas" :
                        piece_type = Piece.PAWN
                    
                    elif "#" in current_line: #we hit the next chunk
                        input.seek(prev_pos) #seek previous position
                        break
                
                    else: #invalid piece_type, raise CorruptedChessFileError
                        raise CorruptedChessFileError("Corrupted chess file")
                        
                    
                    if piece_type != None:
                        if color == "b":
                           piece = Piece(self.game.black, piece_type)
                        
                        elif color == "w":
                           piece = Piece(self.game.white, piece_type)
                    
                        current_split[1] = current_split[1].strip()
                        column_i = Board.column_char_to_integer(current_split[1][0]) #column in integer
                        row_i = Board.row_char_to_integer(current_split[1][1]) #row in integer
                        
                        if (column_i > 7 or row_i > 7) or (column_i < 0 or row_i < 0): #if coordinates are not on the board raise CorruptedChessFileError
                            raise CorruptedChessFileError("Corrupted chess file")
                        
                        if not self.game.board.is_free(column_i, row_i): #check for overlapping pieces, raise CorruptedChessFileError
                            raise CorruptedChessFileError("Corrupted chess file")
                            
                        
                        self.game.board.__setitem__([current_split[1][0],current_split[1][1]], piece)
                
                prev_pos = input.tell() #save previous pointer to go back one step when we hit the next chunk
    
    def read_other_chunk(self, input):
        prev_pos = input.tell()
        for line in input:
            current_line = line
            if "#" in current_line: #we hit the next chunk
                input.seek(prev_pos) #seek previous position
                break
            prev_pos = input.tell()
            
        