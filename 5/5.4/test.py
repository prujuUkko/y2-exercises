import unittest
from io import StringIO

from human_writeable_IO import HumanWriteableIO
from broken_reader import BrokenReader
from corrupted_chess_file_error import CorruptedChessFileError
from piece import Piece

class Test(unittest.TestCase):

    def setUp(self):
        self.human_IO = HumanWriteableIO()

    '''
    IMPORTANT!
    
    The  test method is allowed here to throw the CorruptedChessFileError.
    
    The reasons for this are
    1) we expect the code to work
    2) if the code throws this exception the test will fail 
    
    This is therefore desired behavior for this test. It also removes the problem
    of untestable code in the catch section.
    '''
    
    def test_given_example(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None

        game = self.human_IO.load_game(self.input_file)
        
        self.input_file.close()
        
        self.assertNotEqual(None, game.get_black(), "Loading data failed. Player missing.")
        self.assertEqual("Marko" ,game.get_black().get_name(),  "Loading data failed. Wrong player name.")
        
        
        # Add your own tests, check that the players are ok and that the pieces were correctly placed.   

        
    def test_all_pieces(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a1\nKuningatar      : a2\nTorni:a3\n')
        self.input_file.write('Lahetti    : a4\nRatsu : a5\nSotilas:a6\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None

        game = self.human_IO.load_game(self.input_file)
        
        self.input_file.close()
        
        self.assertNotEqual(None, game.get_black(), "Loading data failed. Player missing.")
        self.assertEqual("Marko" ,game.get_black().get_name(),  "Loading data failed. Wrong player name.")    
    
    
    def test_random_chunk(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('#asdadas\nheheheh\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None

        game = self.human_IO.load_game(self.input_file)
        
        self.input_file.close()
        
        self.assertNotEqual(None, game.get_black(), "Loading data failed. Player missing.")
        self.assertEqual("Marko" ,game.get_black().get_name(),  "Loading data failed. Wrong player name.")
    
    
    def test_not_SHAKKI(self):

        self.input_file = StringIO()
        self.input_file.write('ADASDASD 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
        
    
    def test_not_Tallennustiedosto(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennudsadsa\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
        
        
    def test_missing_black_player(self):
        
        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Valkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
        
    
    def test_missing_white_player(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
        
        
    def test_missing_black_and_white_player(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
    
    
    def test_missing_black_pieces(self):
        
        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
       
    
    def test_missing_white_pieces(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n')
        
        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()


    def test_missing_black_and_white_pieces(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
       
        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
    
    
    def test_invalid_piece_type(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nNakki : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
        
    
    def test_invalid_coordinates(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : x9\nTorni      : a6\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
    
    
    def test_wrong_name1(self):

        self.input_file = StringIO()
        self.input_file.write('SHAKKI 1.222 Tallennustiedosto\n\n')
        self.input_file.write('#Pelin tiedot\n\nTallennettu : 5.7.2001\n')
        self.input_file.write('Musta     : Marko\nValkoinen : Lauri\n\n#Kommentit\n')
        self.input_file.write('\nLaurin revanssipeli, joskin huonosti on menossa...\n')
        self.input_file.write('\n#Musta\n\nKuningas   : a4\nTorni      : a4\n')
        self.input_file.write('Sotilas    : b3\nKuningatar : c8\n\n#Valkoinen\n')
        self.input_file.write('\nKuningas   : d3\nRatsu      : f1')

        self.input_file.seek(0, 0) 

        
        game = None
        
        try:
           game = self.human_IO.load_game(self.input_file)
        except:
            self.assertRaises(CorruptedChessFileError)
        
        self.input_file.close()
    
       
    def testOSException(self):
        '''
        This test was designed to test the catch code in the load_game-method.
        '''

        original_file = StringIO()
        original_file.write('SHAKKI 1.2 Tallennustiedosto\n')
        original_file.write('\n')
        original_file.write('#Pelin tiedot\n')
        original_file.write('\n')
        original_file.write('Tallennettu : 5.7.2001\n')
        original_file.write('Musta     : Marko\n')
        original_file.write('Valkoinen : Lauri\n')
        original_file.write('\n')
        original_file.write('#Kommentit\n')
        original_file.write('\n')
        original_file.write('Laurin revanssipeli, joskin huonosti on menossa...\n')
        original_file.write('\n')
        original_file.write('#Musta\n')
        original_file.write('\n')
        original_file.write('Kuningas   : a4\n')
        original_file.write('Torni      : a6\n')
        original_file.write('Sotilas    : b3\n')
        original_file.write('Kuningatar : c8\n')
        original_file.write('\n')
        original_file.write('#Valkoinen\n')
        original_file.write('\n')
        original_file.write('Kuningas   : d3\n')
        original_file.write('Ratsu      : f1')


        data = original_file.getvalue()
        # Adding a brokenreader allows throwing simulated exceptions
        input_file = BrokenReader(data, 50)

        original_file.close()
        
        check_this = None
        try:
            self.human_IO.load_game(input_file)
        except CorruptedChessFileError as e:
            check_this = e
        
        input_file.close_really()    

        # Note that initially your code does not read past the file header
        # so this test will fail.
        
        self.assertNotEqual(None, check_this, "A CorruptedChessFileError was not thrown.")

 
    def close_silently(self,r):
        try:
            r.close()
        except OSError:
            ''' ignore'''

if __name__ == '__main__':
    unittest.main()
